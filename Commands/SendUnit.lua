function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end


local function ClearState(self)
	--self.threshold = THRESHOLD_DEFAULT
	self.pointmanPosition = Vec3(0,0,0)
end

local threshold =  500

local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self,units,parameter)
	
	local WantedPosition = parameter.position
	local minIndex
	local pointmanID = parameter.unitID[1]
	

	
	--local myUnitID= atlas[1]
	
	
	local cmdID = CMD.MOVE
	
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointmanID)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	SpringGiveOrderToUnit(pointmanID, cmdID, WantedPosition:AsSpringVector(), {})
	if (pointmanPosition:Distance(WantedPosition) < threshold) then
		return SUCCESS
	else
		if(Spring.ValidUnitID(pointmanID))then
			
			return RUNNING								
		else
			return FAILURE
		end
		--Spring.Echo(pointmanPosition:Distance(WantedPosition))
		--SpringGiveOrderToUnit(myUnitID[1], cmdID, WantedPosition:AsSpringVector(), {})
		
		
	end
end

function Reset(self)
	ClearState(self)
end



