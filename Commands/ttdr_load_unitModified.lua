function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "pair",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
			
		}
	}
end


-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition


function Run(self, units, parameter)
 	local pairedUnits = parameter.pair
	--local WantedPosition = parameter.position
	
	
	
	local AtlasID = pairedUnits.AtlasID
	local UnitToRescueID = pairedUnits.UnitToRescueID
    local cmdID = CMD.LOAD_UNITS
	local radius = 50
		
		
		
		local posX, posY, posZ = SpringGetUnitPosition(UnitToRescueID)
		if(Spring.ValidUnitID(UnitToRescueID) == nil)then
			return FAILURE
		end
		
		if(not Spring.ValidUnitID(AtlasID))then
			return FAILURE
		end
	
		SpringGiveOrderToUnit(AtlasID, cmdID, {posX,posY,posZ, radius},  {}) --- load units into bear 
	
		if (#Spring.GetUnitIsTransporting(AtlasID) >= 1) then
			return SUCCESS
		else
			if Spring.ValidUnitID(UnitToRescueID) and Spring.GetUnitHealth(UnitToRescueID) > 0 and Spring.ValidUnitID(AtlasID)then					
				return RUNNING
			else
				return FAILURE
			end
		end
	

    

end