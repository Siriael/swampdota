function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "pair",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "saveAreaCenter",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
						{ 
				name = "depositArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
            },
            { 
				name = "saveAreaRadius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end


function Run(self, units, parameter)

	local pairedUnits = parameter.pair
	local thisUnitID = pairedUnits.AtlasID
	local UnitToRescueID = pairedUnits.UnitToRescueID
	
    local center = parameter.saveAreaCenter 
	 local depositArea = parameter.depositArea
    local radius = parameter.saveAreaRadius
	local atlasGroup = parameter.atlas
   
	--local WantedPosition = parameter.position
	
    local cmdID = CMD.LOAD_UNITS
		
		
    local cmdID = CMD.UNLOAD_UNITS

		local treshold = 150
    
    
    
  --      local posX, posY, posZ = SpringGetUnitPosition(center)
        local posX = center.x
		local posY = center.y
		local posZ = center.z
		
		local pointX, pointY, pointZ = SpringGetUnitPosition(thisUnitID)
		local myposition = Vec3(pointX, pointY, pointZ)
	
	--- load units into atlas 
		if (#Spring.GetUnitIsTransporting(thisUnitID) == 0) then
				
				if ( myposition:Distance(depositArea) < treshold)  then
					return SUCCESS				
				else
						SpringGiveOrderToUnit(thisUnitID, CMD.MOVE, depositArea:AsSpringVector(),{})
					return RUNNING	
				end
		else
			if(Spring.ValidUnitID(thisUnitID))then							
					SpringGiveOrderToUnit(thisUnitID, cmdID, {posX, posY, posZ, radius},{'shift'})
				return RUNNING
			else
				return FAILURE
			end
		end
    

   

end