

local sensorInfo = {
	name = "CenterMass",
	desc = "Return Center Mass of a groupUnits ",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

--************************************************     PriorityQueue     **************************************************************

local SpringGetUnitPosition = Spring.GetUnitPosition


return function( groupUnits )
	
	local totalUnits = #groupUnits
	local total_x = 0
	local total_y = 0
	local total_z = 0
	for i = 1,  #groupUnits do
		local x,y,z = SpringGetUnitPosition(groupUnits[i])
		total_x = total_x + x 
		total_y = total_y + y
		total_z = total_z + z
	end
	
	avg_x = total_x/totalUnits
	avg_y = total_y/totalUnits
	avg_z = total_z/totalUnits
	return Vec3(avg_x,avg_y,avg_z)
	
end





