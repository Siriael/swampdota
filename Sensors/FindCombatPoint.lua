local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(array,EnemyTeamIDs,AllyTeamIDs, range)
	local PossibleFightPoint
	for index,point in pairs(array)do
			local min_x = point.x - range
			local min_z = point.z - range
			
			local max_x = point.x + range
			local max_z = point.z + range
		
		for indexEnemyID , EnemyTeamID in pairs(EnemyTeamIDs) do
					
			
			local enemyUnits = Spring.GetUnitsInRectangle(min_x,min_z,max_x,max_z,EnemyTeamID)
		--	tempenemyUnits = 
			if (#enemyUnits > 0) then
				local FightPoint = { pos = point , index = index }
				return FightPoint
			end	
		end
		
		for indexAllyID , AllyTeamID in pairs(AllyTeamIDs) do	
			
			local allyUnits = Spring.GetUnitsInRectangle(min_x,min_z,max_x,max_z,AllyTeamID)

			if (#allyUnits > 0) then
			
				PossibleFightPoint = { pos = point , index = index }
				
			end	
		end
		
		
		
	end
	return PossibleFightPoint
	

	
end
