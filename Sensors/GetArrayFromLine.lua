--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number






return function(points)
	
	local newArray = {}
	local point_1
	local point_2
--	newArray[1] = points[1].position

	for i = 1 , #points - 1 do
		--Spring.Echo(points[i].position)
		 point_1 = points[i].position
		 point_2 = points[i+1].position		
	
			for a = 0 , 1 , 0.2 do
			
			local	point_3 = point_1 * (1-a) + point_2 * (a)	
				newArray[#newArray +1  ]= point_3
			end      
	end	
	return newArray	
end
		
	
	
	



