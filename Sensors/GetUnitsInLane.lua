local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(point_1,point_2,myTeamID,AllyTeamIDs,EnemyTeamIDs)
	x_min = point_1.x
	z_min = point_1.z
	
	x_max = point_2.x
	z_max = point_2.z
	
	--Check if point_1 and point_2 are not allineated
	
	local lane_width = 600
	local delta = 50
	if ( x_max - x_min < delta) then --points are almost on the same row
		x_min  = x_min - lane_width 
		x_max = x_max + lane_width
		
	end
	
	
	if ( z_max - z_min < delta) then --points are almost on the same column
		z_min  = z_min - lane_width 
		z_max = z_max + lane_width
		
	end	
	local enemyUnits = {}
	
	local offset = 500
	local min_x   
	local min_z
	local max_x
	local max_z
	if(point_1.x<= point_2.x)then
		min_x = point_1.x
		max_x = point_2.x
	else
		min_x = point_2.x
		max_x = point_1.x
	end	
		
	if(point_1.z<= point_2.z)then
		min_z = point_1.z
		max_z = point_2.z
	else
		min_z = point_2.z
		max_z = point_1.z
	end		
	
	min_x = min_x - offset 
	min_z = min_z - offset 
	
	max_x = max_x + offset 
	max_z = max_z + offset 
	for index , EnemyTeamID in pairs (EnemyTeamIDs) do
		tempenemyUnits = Spring.GetUnitsInRectangle(min_x,min_z,max_x,max_z,EnemyTeamID)
			if (#tempenemyUnits > 0) then
				enemyUnits = tempenemyUnits
			end	
	end      

	local allyUnits = {}	  
	for index , AllyTeamID in pairs (AllyTeamIDs) do
		tempallyUnits = Spring.GetUnitsInRectangle(min_x ,min_z,max_x,max_z,AllyTeamID) 
			if (#tempallyUnits > 0) then
				allyUnits = tempallyUnits
			end	
	end
	
	
	local myUnits = Spring.GetUnitsInRectangle(min_x,min_z,max_x,max_z,myTeamID) 
	
	
	local UnitsInLane = {enemyUnits = enemyUnits , allyUnits = allyUnits , myUnits = myUnits}
	return UnitsInLane
	--Spring.Echo(dist)
end
