--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number






return function(array,CPindex,offset)
	return array[CP.index-offset]
	
end





