local sensorInfo = {
	name = "myPosition",
	desc = "Return position of the point unit.",
	author = "DarioLanza",
	date = "2017-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @argument unitID number
-- @description return static position of the first unit
return function(myCM)
	
		local minDist = 10000
	local target = 0
	for indexEnemyTeam, TeamID  in pairs (bb.AllTeamsID.enemyTeams) do
	local EnemyUnits = Spring.GetTeamUnits(TeamID)

	for index,enemyUnitID in pairs (EnemyUnits)do
		local thisUnitDefID = Spring.GetUnitDefID(enemyUnitID)
	
		if(thisUnitDefID ~= nil)then
			local isImmobile  = UnitDefs[thisUnitDefID].isImmobile
			if (isImmobile ~= false)then
				local pointX, pointY, pointZ = SpringGetUnitPosition(enemyUnitID)
				local enemyPos= Vec3(pointX, pointY, pointZ)
				if(enemyPos ~= nil)then
									
					if( myCM:Distance(enemyPos) < minDist)then	
										
						target = enemyUnitID
						minDist = myCM:Distance(enemyPos)
					end
				 else
					
				 end
		
			end
			
		end
	end
	end
	return target
	--Spring.Echo(dist)
end
