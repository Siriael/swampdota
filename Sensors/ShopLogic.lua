--[[function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined position. Group is defined by table of unitID => formationIndex.",
		parameterDefs = {
			{ 
				name = "atlas",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			-- @parameter atlas [table] - mapping unitID => positionIndex
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}

		}
	}
end]]


local sensorInfo = {
	name = "loadUnitsInHovercraft",
	desc = "Returns list of enemy units",
	author = "DarioLanza",
	date = "2010-04-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description load units in an Area
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @argument x number
-- @argument z number
-- @argument radius number






return function()
	local lanes = bb.lanes
	local firstLane  = lanes.firstLane
	local secondLane = lanes.secondLane
	local thirdLane  = lanes.thirdLane
	local info = Sensors.core.missionInfo()
	local pricelist = info.buy
	local listGroups = bb.ListGroups
	
	--Does all the lanes have a radar?
	if(    #listGroups.radars < 1)then
		UnitToBuy = { name = "armseer" , cost = pricelist.armseer ,laneNum = 1}
		return UnitToBuy
	else
		
		if( #listGroups.farks < 3  )then
			UnitToBuy = { name = "armfark" , cost = pricelist.armfark ,laneNum = 1}
			return UnitToBuy
		else
			if( #listGroups.zeus < 3  )then
				UnitToBuy = { name = "armzeus" , cost = pricelist.armzeus ,laneNum = 1}
				return UnitToBuy
			else			
				if( #listGroups.armmarts < 10  )then
					UnitToBuy = { name = "armmart" , cost = pricelist.armmart ,laneNum = 1}
					return UnitToBuy
				end
			end
		end
	end

	
end





